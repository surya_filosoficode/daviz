<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="DashForge">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/dashforge">
    <meta property="og:title" content="DashForge">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>assets/theme/assets/img/favicon.png">

    <title>Dashboard Admin</title>

    <!-- vendor css -->
    <link href="<?= base_url() ?>assets/theme/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/theme/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/theme/assets/css/dashforge.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/theme/assets/css/dashforge.auth.css">

    <!-- sweetalert2 -->
    <!-- sweetalert2.min.css -->
    <!-- sweetalert2.min.js -->
    <!-- sweetalert2 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/theme/lib/sweetalert2/sweetalert2.min.css">
    <script src="<?= base_url() ?>assets/theme/lib/sweetalert2/sweetalert2.min.js"></script>

    <script src="<?= base_url() ?>assets/js/jquery-3.2.1.js"></script>
</head>

<body>
    <div class="content content-fixed content-auth">
        <div class="container">
            <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
                <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">
                    <div class="wd-100p">
                        <h3 class="tx-color-01 mg-b-5">Sign In</h3>
                        <p class="tx-color-03 tx-16 mg-b-40">Welcome back! Please signin to continue.</p>
                        <form id="loginform">
                            <div class="form-group">
                                <label>Email address</label>
                                <input class="form-control" type="text" name="username" id="username" required="" placeholder="yourname@yourmail.com">
                                <p id="msg_username" style="color: red;font-size: small;"></p>
                            </div>
                            <div class="form-group">
                                <div class="d-flex justify-content-between mg-b-5">
                                    <label class="mg-b-0-f">Password</label>
                                    <a href="" class="tx-13">Forgot password?</a>
                                </div>
                                <input class="form-control" type="password" name="password" id="password" required="" placeholder="Enter your password">
                                <p id="msg_password" style="color: red;font-size: small;"></p>
                            </div>
                            <button type="submit" class="btn btn-brand-02 btn-block" id="login">Sign In</button>
                        </form>
                    </div>
                </div><!-- sign-wrapper -->
            </div><!-- media -->
        </div><!-- container -->
    </div><!-- content -->

    <footer class="footer">
        <div>
            <span>&copy; 2019 DashForge v1.0.0. </span>
            <span>Created by <a href="http://themepixels.me">ThemePixels</a></span>
        </div>
        <div>
            <nav class="nav">
                <a href="https://themeforest.net/licenses/standard" class="nav-link">Licenses</a>
                <a href="<?= base_url() ?>assets/theme/change-log.html" class="nav-link">Change Log</a>
                <a href="https://discordapp.com/invite/RYqkVuw" class="nav-link">Get Help</a>
            </nav>
        </div>
    </footer>

    <script src="<?= base_url() ?>assets/theme/lib/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/theme/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url() ?>assets/theme/lib/feather-icons/feather.min.js"></script>
    <script src="<?= base_url() ?>assets/theme/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <script src="<?= base_url() ?>assets/theme/assets/js/dashforge.js"></script>

    <!-- append theme customizer -->
    <script src="<?= base_url() ?>assets/theme/lib/js-cookie/js.cookie.js"></script>
    <script src="<?= base_url() ?>assets/theme/assets/js/dashforge.settings.js"></script>

    <script src="<?= base_url() ?>assets/js/custom/main_custom.js"></script>
    <script type="text/javascript">
        $(function() {
            'use script'

            window.darkMode = function() {
                $('.btn-white').addClass('btn-dark').removeClass('btn-white');
            }

            window.lightMode = function() {
                $('.btn-dark').addClass('btn-white').removeClass('btn-dark');
            }

            var hasMode = Cookies.get('df-mode');
            if (hasMode === 'dark') {
                darkMode();
            } else {
                lightMode();
            }
        })

        $(document).ready(function() {
            document.getElementById("username").focus();
        });

        function send() {
            var data_main = $("#loginform").serialize();
            // console.log(data_main);
            $.ajax({
                url: "<?php echo base_url() . "log/auth"; ?>",
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: 'application/x-www-form-urlencoded',
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res, textStatus, jQxhr) {
                    response_login(res);
                }
            });

        }

        $("#loginform").submit(function(e) {
            e.preventDefault();
            send();

        });

        $("#login").click(function(e) {
            e.preventDefault();
            send();
        });

        function response_login(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url() . "login"); ?>");
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");

                $("#msg_username").html(detail_msg.username);
                $("#msg_password").html(detail_msg.password);
            }
        }
    </script>
</body>

</html>