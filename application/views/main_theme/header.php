<header class="navbar navbar-header navbar-header-fixed">
    <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
    <div class="navbar-brand">
        <a href="<?= base_url() ?>assets/theme/index.html" class="df-logo">DA<span>VIZ</span></a>
    </div><!-- navbar-brand -->
    <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
            <a href="<?= base_url() ?>assets/theme/index.html" class="df-logo">DA<span>VIZ</span></a>
            <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
        <ul class="nav navbar-menu">
            <li class="nav-label pd-l-20 pd-lg-l-25 d-lg-none">Main Navigation</li>
            <li class="nav-item with-sub active">
                <a href="" class="nav-link"><i data-feather="pie-chart"></i> Home</a>
                <ul class="navbar-menu-sub">
                    <li class="nav-sub-item"><a href="dashboard-one.html" class="nav-sub-link"><i data-feather="bar-chart-2"></i>Sales Monitoring</a></li>
                </ul>
            </li>
            <!-- <li class="nav-item with-sub">
                <a href="" class="nav-link"><i data-feather="package"></i> Admin</a>
            </li> -->
            <li class="nav-item"><a href="<?= base_url() ?>assets/theme/components/" class="nav-link"><i data-feather="box"></i> Admin</a></li>
            <li class="nav-item"><a href="<?= base_url() ?>assets/theme/components/" class="nav-link"><i data-feather="box"></i> Menu</a></li>
            <!-- <li class="nav-item"><a href="<?= base_url() ?>assets/theme/collections/" class="nav-link"><i data-feather="archive"></i> Collections</a></li> -->
        </ul>
    </div><!-- navbar-menu-wrapper -->
    <div class="navbar-right">
        <a id="navbarSearch" href="" class="search-link"><i data-feather="search"></i></a>


        <div class="dropdown dropdown-profile">
            <a href="" class="dropdown-link" data-toggle="dropdown" data-display="static">
                <div class="avatar avatar-sm"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
            </a><!-- dropdown-link -->
            <div class="dropdown-menu dropdown-menu-right tx-13">
                <div class="avatar avatar-lg mg-b-15"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
                <h6 class="tx-semibold mg-b-5">Surya Hanggara</h6>
                <p class="mg-b-25 tx-12 tx-color-03">Contribuutor</p>

                <a href="" class="dropdown-item"><i data-feather="edit-3"></i> Edit Profile</a>
                <a href="page-signin.html" class="dropdown-item"><i data-feather="log-out"></i>Sign Out</a>
            </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
    </div><!-- navbar-right -->
    <div class="navbar-search">
        <div class="navbar-search-header">
            <input type="search" class="form-control" placeholder="Type and hit enter to search...">
            <button class="btn"><i data-feather="search"></i></button>
            <a id="navbarSearchClose" href="" class="link-03 mg-l-5 mg-lg-l-10"><i data-feather="x"></i></a>
        </div><!-- navbar-search-header -->
    </div><!-- navbar-search -->
</header><!-- navbar -->