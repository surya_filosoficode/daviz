 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate_qrcode {
    public function __construct(){
        $this->load->library('session');
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function get_qr_code($code_contents){
        include(APPPATH.'libraries/qrlib.php');
        $tempDir = "./assets/qr/";
        
        // $codeContents = 'surya hanggara';
        QRcode::png($code_contents, $tempDir.$code_contents.'.png', QR_ECLEVEL_H, 10);

        return $code_contents;
    }

}